from django import forms 
from django.contrib.auth.models import User 
from django.contrib.auth.forms import UserCreationForm
from .models import Profile, UserRss

class DateInput(forms.DateInput):
    input_type = 'date'

class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email', 'password1','password2']

class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email']

class ProfileUpdateForm(forms.ModelForm):
 
    class Meta:
        model = Profile
        fields = ['image','bio','interest','location','birth_date','stack','github_link','linkedin_link','gitlab_link','twitter_link','website_link']
        labels = {'image':'Change your profile picture', 'stack':"Pick stack you're a familiar with. Ctrl/Cmd + click to select more than one."}
        widgets = {'birth_date': DateInput()}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['image'].widget.attrs['title'] = 'test'
        self.fields['bio'].widget.attrs['placeholder'] = 'Write something short about you!'
        self.fields['interest'].widget.attrs['placeholder'] = 'Give us your main interest. Only one!'

class RssCreateForm(forms.ModelForm):
    
    class Meta:
        model = UserRss
        fields = ['feed_name', 'feed_url',]
