from django.shortcuts import render, redirect, get_object_or_404, HttpResponse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm, RssCreateForm
from blog.models import Post, User, RssSettings, Like, Save
from allauth.socialaccount.models import SocialAccount, SocialToken
from actions.models import Action
from .models import UserRss
from github import Github
from blog.views_methods import get_actions, hacker_news_feed
import requests
import json


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Hello { username }, your account has been created! You are now able to log in.')
            return redirect('login')
    
    else:
        form = UserRegisterForm()
    return render(request, 'users/register.html', {'form': form})

@login_required
def profile(request):

    user = request.user
    posts = Post.objects.filter(author=user).order_by('-date_posted')[:6]

    actions = get_actions(request)

    feed = hacker_news_feed()

    return render(request,'users/profile.html',
                                                {'posts': posts,
                                                'user': user,
                                                'likes': Like.objects.filter(owner=request.user).order_by('-liked_time')[:5],
                                                'saved': Save.objects.filter(owner=request.user).order_by('-liked_time')[:5],
                                                'actions': actions,
                                                'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted')[:6],
                                                'discussion': Post.objects.filter(tags__tag_name='discussion')[:6],
                                                'feed':feed
                                                })


@login_required
def user_settings(request):
    # Update profile

    user = request.user
    posts = Post.objects.filter(author=user).order_by('date_posted')

    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST, 
                                   request.FILES, 
                                   instance=request.user.profile)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f'Your account has been updated!')
            return redirect('profile')

    else: 
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)

    context = {
        'u_form': u_form,
        'p_form': p_form,
        'posts': posts,
    }
    # render profile
    return render(request, 'users/settings.html', context)

@login_required
def settings_github(request):

    if SocialAccount.objects.filter(user=request.user, provider="github").exists():
        social = SocialAccount.objects.get(user=request.user, provider="github")
        access_token = SocialToken.objects.get(account__user=request.user, account__provider='github')
        g = Github(str(access_token))

        repolist = []
        for repo in g.get_user().get_repos():
            repolist.append(repo.name)

        context = {
            "token": access_token,
            "repolist": repolist,
            "social": social,
        }

        return render(request, 'users/settings_github.html', context)

    return render(request, 'users/settings_github.html')

@login_required
def settings_rss(request):
    
    user = request.user
    
    rss = UserRss.objects.filter(feed_author=user)

    rss_des = RssSettings.objects.all()
    rss_des = rss_des.get(pk=1)

    new_rss = None

    if request.method == 'POST':
        rss_form = RssCreateForm(data=request.POST)

        if rss_form.is_valid():
            new_rss = rss_form.save(commit=False)
            new_rss.feed_author = user
            new_rss.save()

            messages.success(request, f'Your feed has been added!')
            return redirect('news')

    else:
        rss_form = RssCreateForm()

    context = {
        'new_rss':new_rss,
        'rss_form':rss_form,
        'rss': rss,
        'rss_des': rss_des
    }

    return render(request, 'users/settings_rss.html', context)
