# Generated by Django 2.2 on 2020-04-06 05:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='stack',
            name='stack_image',
        ),
        migrations.AddField(
            model_name='stack',
            name='stack_thumb',
            field=models.ImageField(null=True, upload_to='stack_logos'),
        ),
    ]
