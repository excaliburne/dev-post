from django.contrib import admin
from .models import Profile, UserRss, Stack

admin.site.register(Profile)
admin.site.register(UserRss)
admin.site.register(Stack)