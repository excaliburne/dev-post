from django.db import models
from django.contrib.auth.models import User 
from PIL import Image
import io
from django.core.files.storage import default_storage as storage
from io import BytesIO
from django.core.files.base import ContentFile
from resizeimage import resizeimage

class UserRss(models.Model):
    feed_name = models.CharField(max_length=30)
    feed_url = models.URLField(null=True, blank=True)
    feed_author = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return "'{}' added by {}".format(self.feed_name, self.feed_author)

class Stack(models.Model):
    stack_name = models.CharField(max_length=30)
    # stack_image = models.CharField(max_length=120)
    stack_thumb = models.ImageField(upload_to='stack_logos', null=False)

    class Meta:
        ordering = ('stack_name',)

    def __str__(self):
        return self.stack_name

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, max_length=30)
    bio = models.TextField(max_length=140, null=True, blank=True)
    interest = models.CharField(max_length=30, null=True, blank=True)
    location = models.CharField(max_length=30, blank=True)
    linkedin_link = models.URLField(null=True, blank=True)
    github_link = models.URLField(null=True, blank=True)
    gitlab_link = models.URLField(null=True, blank=True)
    twitter_link = models.URLField(null=True, blank=True)
    website_link = models.URLField(null=True, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    image = models.ImageField(default='profile_pics/default.jpg', upload_to='profile_pics')
    stack = models.ManyToManyField(Stack)
    
    # prints out profile on admin
    def __str__(self):
        return f'{self.user.username} Profile'
    
    '''
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        img_read = storage.open(self.image.name, 'r')
        img = Image.open(img_read)

        if img.height > 1000 or img.width > 1000:
            output_size = (300, 300)
            img.thumbnail(output_size)
            in_mem_file = io.BytesIO()
            img.save(in_mem_file, format='JPEG')
            img_write = storage.open(self.image.name, 'w+')
            img_write.write(in_mem_file.getvalue())
            img_write.close()

        img_read.close()
    '''
