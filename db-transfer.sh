# copy/import data from heroku postgres to localhost pg database

# useful for copying admin's work on live site into local database to reproduce errors
# https://devcenter.heroku.com/articles/heroku-postgres-import-export

# take heroku pg snapshot and download
heroku pg:backups:capture
heroku pg:backups:download

# load the dump into local postgres database, assuming $DATABASE_URL set locally
export DB_NAME=$(echo $DATABASE_URL | sed 's:.*/::')
pg_restore --verbose --clean --no-acl --no-owner -h localhost -d DOTpost latest.dump

rm latest.dump