# Introduction

This is a personal/practice project. It's a rich-featured community-like website about tech related stuff.
Note: This app is not meant to be used in production now and some information are in this repo for demonstration purpose. This project is still at an early development stage.

Version 0.9 is deployed on Heroku: https://excdevpost.herokuapp.com/

Want to try it? Register an account and login!

## Features
- Login, logout and register
- Multi-users
- Publish and update
- Live feed
- Like and save
- Comments
- Follow other users
- Chat with other users when mutual following
- OAuth login and registration
- Ability to build custom RSS feed
- Polls
- Retrieving github repositories by access token if Github account linked

## Tech used
- Python3
- Django 2.2.0
- Javascript/HTML/CSS
- jQuery
- django-allauth
- PyGithub
- requests
- redis
- feedparser
- django-channels
- AWS S3

## Snippets

Home page <br/>
<img src="https://i.imgur.com/FfePgjC.png" width="500" height="300"/>

Right sidebar/Feed <br/>
<img src="https://i.imgur.com/ZevLgEn.png" width="500" height="300"/>

Personal profile <br/>
<img src="https://i.imgur.com/rF83AAU.png" width="500" height="300"/>

Users <br/>
<img src="https://i.imgur.com/vyPkMzF.png" width="500" height="300"/>

See other users profile and follow them <br/>
<img src="https://i.imgur.com/kI8sRtE.png" width="500" height="300"/>

Settings <br/>
<img src="https://i.imgur.com/JO1rimE.png" width="500" height="300"/>

Connect your Github account <br/>
<img src="https://i.imgur.com/2r9uA4J.png" width="500" height="270"/>

Chat in real time with people you follow <br/>
<img src="https://i.imgur.com/V4yNcXr.png" width="500" height="250"/>

### Database
<img src="https://i.imgur.com/RdW2G91.png" width="850" height="420"/>


## Documentation

### Install on your own server

`git clone https://gitlab.com/excaliburne/dev-post.git` <br/>
`pip3 install -r requirements.txt` <br/>
`python3 manage.py migrate` <br/>
`python3 manage.py runserver` <br/>

### Chat, channels & websockets

`brew install redis` <br/>
`brew services start redis`