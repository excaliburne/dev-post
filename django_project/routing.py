from django.conf.urls import url 
from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.security.websocket import AllowedHostsOriginValidator, OriginValidator

from chat.consumers import ChatConsumer

### TEST
from channels.staticfiles import StaticFilesConsumer
from chat import consumers


application = ProtocolTypeRouter({
    'websocket': AllowedHostsOriginValidator(
        AuthMiddlewareStack(
           URLRouter(
               [
                   url(r"^chat/(?P<username>[\w.@+-]+)/$", ChatConsumer),
               ]
           ) 
        )
    )
})

### TEST
channel_routing = {
    # This makes Django serve static files from settings.STATIC_URL, similar
    # to django.views.static.serve. This isn't ideal (not exactly production
    # quality) but it works for a minimal example.
    'http.request': StaticFilesConsumer(),

    # Wire up websocket channels to our consumers:
    'websocket.connect': consumers.websocket_connect,
    'websocket.receive': consumers.websocket_receive,
    'websocket.disconnect': consumers.websocket_disconnect,
}