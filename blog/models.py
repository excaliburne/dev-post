from django.db import models
from django.utils import timezone
from datetime import datetime
from django.contrib.auth.models import User
from django.urls import reverse
from tinymce import HTMLField
from django.core.validators import (MaxValueValidator, MinValueValidator)
# from django.utils.translation import ugettext_lazy as _
# from trix.fields import TrixField


########### Post models logic  ###############

class Tags(models.Model):
    tag_name = models.CharField(max_length=50)
    # description = models.TextField(max_length=550, default="")
    description = HTMLField()

    class Meta:
        ordering = ('tag_name',)

    def __str__(self):
        return self.tag_name


class Post(models.Model):
    title = models.CharField(max_length=100)
    # content = TrixField('content')
    content = HTMLField()
    date_posted = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tags, blank=True)


    def __str__(self):
        return self.title
    
    # create an url for the post
    def get_absolute_url(self):
        return reverse('post-detail', kwargs={'pk': self.pk})
      
    def get_like_url(self, *args, **kwargs):
      return reverse('post-likes', kwargs={'id': self.pk})
  
    def get_unlike_url(self, *args, **kwargs):
      return reverse('post-unlikes', kwargs={'id': self.pk})

    def get_save_url(self, *args, **kwargs):
      return reverse('post-save', kwargs={'id': self.pk})
  
    def get_unsave_url(self, *args, **kwargs):
      return reverse('post-unsave', kwargs={'id': self.pk})

User.add_to_class('published',
                  models.ManyToManyField('self',
                                         through=Post,
                                         related_name='publishers',
                                         symmetrical=False))



class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments')
    author = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    body = models.TextField(max_length=140, default="")
    created_on = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=False)

    class Meta:
        ordering = ['created_on']

    def __str__(self):
        return 'Comment {} by {}'.format(self.body, self.author)

##### LIKE ######

# Like Manager
class LikeManager(models.Manager):
  def find_is_liked(self, post, user):
    return self.filter(post=post, owner=user)

  def create_like(self, post, user):
    like = self.create(post=post, owner=user)
    like.save()
  


# LIKE MODEL
class Like(models.Model):
  post = models.ForeignKey(
    Post,
    default=1,
    on_delete=models.CASCADE
  )
  owner = models.ForeignKey(
    User,
    default=1,
    on_delete=models.CASCADE
  )
  liked_time = models.DateTimeField(auto_now_add=True)

  objects = LikeManager()

  def __str__(self, *args, **kwargs):
    return self.post.title

###### UNLIKE #######

# UNLIKE MANAGER
class UnlikeManager(models.Manager):
  def find_is_unliked(self, post, user):
    return self.filter(post=post, owner=user)
  
  def create_unlike(self, post, user):
    unlike = self.create(post=post, owner=user)
    unlike.save()


# UNLIKE MODEL
class Unlike(models.Model):
  post = models.ForeignKey(
    Post,
    default=1,
    on_delete=models.CASCADE
  )
  owner = models.ForeignKey(
    User,
    default=1,
    on_delete=models.CASCADE
  )

  objects = UnlikeManager()

  def __str__(self, *args, **kwargs):
    return self.post.title

##### SAVE POST ######

# Like Manager
class SaveManager(models.Manager):
  def find_is_saved(self, post, user):
    return self.filter(post=post, owner=user)

  def create_save(self, post, user):
    like = self.create(post=post, owner=user)
    like.save()


# LIKE MODEL
class Save(models.Model):
  post = models.ForeignKey(
    Post,
    default=1,
    on_delete=models.CASCADE
  )
  owner = models.ForeignKey(
    User,
    default=1,
    on_delete=models.CASCADE
  )
  liked_time = models.DateTimeField(auto_now_add=True)

  objects = SaveManager()

  def __str__(self, *args, **kwargs):
    return self.post.title

###### UNSAVE#######

class UnsaveManager(models.Manager):
  def find_is_unsaved(self, post, user):
    return self.filter(post=post, owner=user)
  
  def create_unsave(self, post, user):
    unlike = self.create(post=post, owner=user)
    unlike.save()


# UNLIKE MODEL
class Unsave(models.Model):
  post = models.ForeignKey(
    Post,
    default=1,
    on_delete=models.CASCADE
  )
  owner = models.ForeignKey(
    User,
    default=1,
    on_delete=models.CASCADE
  )

  objects = UnsaveManager()

  def __str__(self, *args, **kwargs):
    return self.post.title


############ FOLLOW ###################

class Contact(models.Model):
    user_from = models.ForeignKey(User,
                                  related_name='rel_from_set',
                                  on_delete=models.CASCADE)
    user_to = models.ForeignKey(User,
                                related_name='rel_to_set',
                                on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True,
                                   db_index=True)

    class Meta:
        ordering = ['-created',]

    def __str__(self):
        return '{} follows {}'.format(self.user_from,
                                      self.user_to)

User.add_to_class('following',
                  models.ManyToManyField('self',
                                         through=Contact,
                                         related_name='followers',
                                         symmetrical=False))


############ Pages admin ##############

class RssSettings(models.Model):
    rss_desciption = HTMLField()

    def __str__(self):
        return self.rss_desciption

class PublishPage(models.Model):
    description = HTMLField()

    def __str__(self):
      return self.description

class AboutPage(models.Model):
    content = HTMLField()

    def __str__(self):
        return self.content


################ Poll ################

class PollQuestion(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.question_text


class PollChoice(models.Model):
    question = models.ForeignKey(PollQuestion, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text


class TestTable(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class TestTableTwo(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
      return self.name
