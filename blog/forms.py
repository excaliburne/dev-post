from django import forms
from .models import Comment, Post
# from trix.widgets import TrixEditor
from tinymce.widgets import TinyMCE
from django.core.mail import send_mail 
import logging

logger = logging.getLogger(__name__)

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('body',)
        labels = {'body':''}

class PostCreateForm(forms.ModelForm):
    # content = forms.CharField(widget=TrixEditor)
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 60, 'rows': 30}))

    class Meta:
        model = Post 
        fields = ('title','content','tags',)
        labels = {'tags':'Ctrl/Cmd + click to select more than one'}

class ContactForm(forms.Form):
    name = forms.CharField(label='Your name', max_length=100)
    message = forms.CharField(max_length=600, widget=forms.Textarea)
    from_email = forms.EmailField(required=True)
    subject = forms.CharField(max_length=200)

    """
    def send_mail(self):
        logger.info("Sending email to customer service")
        message = "From: {0}\n{1}".format( 
            self.cleaned_data["name"], 
            self.cleaned_data["message"],
        ) 
        send_mail(  
                    
                    "Site message", 
                    "site@booktime.domain", 
                    ["customerservice@booktime.domain"], 
                    fail_silently=False,
                )
    """