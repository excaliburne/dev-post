from actions.models import Action
from django.contrib.auth.models import User
import feedparser

def get_actions(request):
  actions = Action.objects.exclude(user=request.user) 

  following_ids = request.user.following.values_list('id', flat=True)
  if following_ids:
    # If user is following others, retrieve only their actions 
    actions = actions.filter(user_id__in=following_ids)
  actions = actions.select_related('user', 'user__profile')\
              .prefetch_related('target')[:5]

  return actions


# retrieve name id that current logged user follow
def get_follow_names(request):

  following_ids = request.user.following.values_list('id', flat=True)
  if following_ids:
    names = User.objects.filter(id__in=following_ids)

  return names

def hacker_news_feed():
  hackernews_url = "https://hnrss.org/newest"
  feed = feedparser.parse(hackernews_url)
  return feed

