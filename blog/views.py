from django.shortcuts import render, get_object_or_404, Http404, HttpResponseRedirect, reverse, redirect
from django.http import JsonResponse
from django.views.decorators.http import require_POST
from .forms import PostCreateForm, CommentForm, ContactForm
from .models import Post, Comment, PollQuestion, PollChoice, AboutPage, Like, Unlike, Tags, PublishPage, Contact, Save, Unsave
from actions.models import Action
from users.models import Profile, UserRss
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView, View
from django.contrib import messages
from actions.utils import create_action
from django.db.models import Q 
from blog.config import pagination
from tinymce import HTMLField
import feedparser
from django.utils import timezone
import urllib
from django.core.mail import send_mail
from django.db.models import Count
from blog.views_methods import get_actions, hacker_news_feed

class PostListView(ListView):
    model = Post
    template_name = 'blog/home_dev.html'
    context_object_name = 'posts'
    ordering = ['-date_posted']

    ## paginatorf
    paginate_by = 8

    def get_context_data(self, **kwargs):
        context = super(PostListView, self).get_context_data(**kwargs)

        if self.request.user.is_authenticated:

          actions = get_actions(self.request)

        else:
          pass
        
        feed = hacker_news_feed()

        if self.request.user.is_authenticated:
          context.update({
              'users': User.objects.all(),
              'latest_user': User.objects.order_by('-date_joined')[:5],
              'likes': Like.objects.filter(owner=self.request.user).order_by('-liked_time')[:5],
              'saved': Save.objects.filter(owner=self.request.user).order_by('-liked_time')[:5],
              'actions': actions, 
              'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted')[:6],
              'discussion': Post.objects.filter(tags__tag_name='discussion')[:6],
              'feed':feed
          })
          return context
        else:
          context.update({
              'users': User.objects.all(),
              'latest_user': User.objects.order_by('-date_joined')[:5],
              'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted'),
              'discussion': Post.objects.filter(tags__tag_name='discussion')[:6],
              'feed':feed
          })
          return context

# get post from certain user
class UserPostListView(ListView):
    model = Post
    template_name = 'blog/user_posts.html'
    context_object_name = 'posts'

    ## paginator
    paginate_by = 8
    
    def get_queryset(self):
        user = get_object_or_404(User, username=self.kwargs.get('username'))
        return Post.objects.filter(author=user).order_by('date_posted')


@login_required
def user_detail(request, username):
    user = get_object_or_404(User,
                             username=username)
    posts = Post.objects.filter(author=user).order_by('-date_posted')[:6]
    
    comments = Comment.objects.filter(author=user).order_by('-created_on')[:5]

    if request.user.is_authenticated:

      actions = get_actions(request)

    else:
      pass

    feed = hacker_news_feed()

    return render(request,
                  'blog/user_profile_dev.html',
                  {'posts': posts,
                   'user': user,
                   'actions': actions, 
                   'likes': Like.objects.filter(owner=request.user).order_by('-liked_time')[:5],
                   'saved': Save.objects.filter(owner=request.user).order_by('-liked_time')[:5],
                   'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted')[:6],
                   'discussion': Post.objects.filter(tags__tag_name='discussion')[:6],
                   'comments': comments,
                   'feed':feed
                    })


### Display user list page
@login_required
def user_list(request):
    users = User.objects.all()
    feed = hacker_news_feed()
    actions = get_actions(request)
    
    return render(request,
                  'blog/users_list_dev.html',
                  {'section': 'people',
                   'users': users,
                   'likes': Like.objects.filter(owner=request.user).order_by('-liked_time')[:5],
                   'saved': Save.objects.filter(owner=request.user).order_by('-liked_time')[:5],
                   'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted')[:6],
                   'discussion': Post.objects.filter(tags__tag_name='discussion')[:6],
                   'feed':feed,
                   'actions':actions
                   })



# view post
def post_detail(request, pk):
    user = request.user
    template_name = 'blog/post_detail_dev.html'
    post = get_object_or_404(Post, id=pk)
    comments = post.comments.filter(active=True)
    new_comment = None

    if user.is_authenticated:

      is_liked = Like.objects.find_is_liked(
        post,
        request.user
      )

      is_unliked = Unlike.objects.find_is_unliked(
          post,
          request.user
        )

      is_saved = Save.objects.find_is_saved(
        post,
        request.user
      )

      get_likes = Like.objects.filter(owner=request.user).order_by('-liked_time')[:5]
      get_saved = Save.objects.filter(owner=request.user).order_by('-liked_time')[:5]

      actions = get_actions(request)

    # Comment posted
    if request.method == 'POST':
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():

            # Create Comment object but don't save to database yet
            new_comment = comment_form.save(commit=False)
            # Assign the current post to the comment
            new_comment.post = post
            new_comment.author = request.user
            create_action(request.user, 'commented your post', )
            # Save the comment to the database
            new_comment.save()

    else:
        comment_form = CommentForm()
    
    post_tags_ids = post.tags.values_list('id', flat=True) 
    similar_posts = Post.objects.filter(tags__in=post_tags_ids).exclude(id=post.id)
    similar_posts = similar_posts.annotate(same_tags=Count('tags')).order_by('-date_posted')[:4]

    feed = hacker_news_feed()
    
    if user.is_authenticated:
      return render(request, template_name, {'post': post,
                                           'comments': comments,
                                           'new_comment': new_comment,
                                           'comment_form': comment_form,
                                           'is_liked': is_liked,
                                           'is_unliked': is_unliked,
                                           'is_saved': is_saved,
                                           'likes': get_likes,
                                           'saved': get_saved,
                                           'actions':actions,
                                           'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted')[:6],
                                           'discussion': Post.objects.filter(tags__tag_name='discussion')[:6],
                                           'similar_posts': similar_posts,
                                           'feed':feed
                                           })
    else:
      return render(request, template_name, {'post': post,
                                           'comments': comments,
                                           'new_comment': new_comment,
                                           'comment_form': comment_form,
                                           'discussion': Post.objects.filter(tags__tag_name='discussion')[:6],
                                           'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted')[:6],
                                           'feed':feed
                                           })


# create a post
class PostCreateView(LoginRequiredMixin,CreateView):
    model = Post
    form_class = PostCreateForm

    def form_valid(self, form):
        form.instance.author = self.request.user
        create_action(self.request.user, 'has published a new post!')
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(PostCreateView, self).get_context_data(**kwargs)

        feed = hacker_news_feed()

        if self.request.user.is_authenticated:

          actions = get_actions(self.request)
          
        else:
          pass
          

        if self.request.user.is_authenticated:

          context.update({
              'publish_page_description': PublishPage.objects.get(id=1),
              'likes': Like.objects.filter(owner=self.request.user).order_by('-liked_time')[:5],
              'saved': Save.objects.filter(owner=self.request.user).order_by('-liked_time')[:5],
              'actions': actions, 
              'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted')[:6],
              'discussion': Post.objects.filter(tags__tag_name='discussion')[:6],
              'feed':feed
          })
          return context

        else:
          context.update({
              'publish_page_description': PublishPage.objects.get(pk=1),
          })
          return context


# update a post
class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ['title','content','tags']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)
    
    # prevent users to edit any posts
    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False
      
    def get_context_data(self, **kwargs):
        context = super(PostUpdateView, self).get_context_data(**kwargs)

        actions = get_actions(self.request)

        context.update({
              'publish_page_description': PublishPage.objects.get(pk=1),
              'likes': Like.objects.filter(owner=self.request.user).order_by('-liked_time')[:5],
              'saved': Save.objects.filter(owner=self.request.user).order_by('-liked_time')[:5],
              'actions': actions, 
        })
        return context

# delete a post
class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    success_url = '/'

    # prevent users to edit any posts
    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False

###### LIKE / DISLIKE #######

class PostLikeView(LoginRequiredMixin, View):
  lookup = 'id'

  def get_object(self, *args, **kwargs):
    return get_object_or_404(Post, pk=self.kwargs.get(self.lookup))

  def get(self, request, id=None, *args, **kwargs):
    is_liked = Like.objects.find_is_liked(
      self.get_object(),
      request.user
    )
    
    # like = Post.objects.get(id=like_id)

    if is_liked.exists():
      # messages.error(request, 'Post has already been liked!')
      uncreate_like = Like.objects.filter(post=id, owner=request.user)
      uncreate_like.delete()
      return redirect('post-detail', pk=id)

    else:
      is_unliked = Unlike.objects.find_is_unliked(
        self.get_object(),
        request.user
      )

      if is_unliked.exists():
        is_unliked.delete()
        Like.objects.create_like(self.get_object(), request.user)
        # messages.success(request, 'Post has been liked!') 
        return redirect('post-detail', pk=id)
      else:
        Like.objects.create_like(self.get_object(), request.user)
        # messages.success(request, 'Post has been liked!')  
        return redirect('post-detail', pk=id)


# POST UNLIKE VIEW
class PostUnlikeView(LoginRequiredMixin, View):
  lookup = 'id'

  def get_object(self, *args, **kwargs):
    return get_object_or_404(Post, pk=self.kwargs.get(self.lookup))

  def get(self, request, id=None, *args, **kwargs):
    is_unliked = Unlike.objects.find_is_unliked(
      self.get_object(),
      request.user
    )

    if is_unliked.exists():
      # messages.error(request, 'Post has already been unliked!')
      uncreate_dislike = Unlike.objects.filter(post=id, owner=request.user)
      uncreate_dislike.delete()
      return redirect('post-detail', pk=id)
    else:
      is_liked = Like.objects.find_is_liked(
      self.get_object(),
      request.user
    )
      if is_liked.exists():
        is_liked.delete()
        Unlike.objects.create_unlike(self.get_object(), request.user)
        # messages.success(request, 'Post has been unliked!')
        return redirect('post-detail', pk=id)
      else:
        Unlike.objects.create_unlike(self.get_object(), request.user)
        # messages.success(request, 'Post has been unliked!')
        return redirect('post-detail', pk=id)


###### SAVE / UNSAVE 

class PostSaveView(LoginRequiredMixin, View):
  lookup = 'id'

  def get_object(self, *args, **kwargs):
    return get_object_or_404(Post, pk=self.kwargs.get(self.lookup))

  def get(self, request, id=None, *args, **kwargs):
    is_saved = Save.objects.find_is_saved(
      self.get_object(),
      request.user
    )

    if is_saved.exists():
      # messages.error(request, 'Post has already been saved!')
      return redirect('post-detail', pk=id)

    else:
      is_unsaved = Unsave.objects.find_is_unsaved(
        self.get_object(),
        request.user
      )

      if is_unsaved.exists():
        is_unsaved.delete()
        Save.objects.create_save(self.get_object(), request.user)
        # messages.success(request, 'Post has been saved!')
        return redirect('post-detail', pk=id)
      else:
        Save.objects.create_save(self.get_object(), request.user)
        # messages.success(request, 'Post has been saved!')
        return redirect('post-detail', pk=id)


# POST UNLIKE VIEW
class PostUnSaveView(LoginRequiredMixin, View):
  lookup = 'id'

  def get_object(self, *args, **kwargs):
    return get_object_or_404(Post, pk=self.kwargs.get(self.lookup))

  def get(self, request, id=None, *args, **kwargs):
    is_unsaved = Unsave.objects.find_is_unsaved(
      self.get_object(),
      request.user
    )

    if is_unsaved.exists():
      # messages.error(request, 'Post has already been unsaved!')
      return redirect('post-detail', pk=id)
    else:
      is_saved = Save.objects.find_is_saved(
      self.get_object(),
      request.user
    )
      if is_saved.exists():
        is_saved.delete()
        Unsave.objects.create_unsave(self.get_object(), request.user)
        # messages.success(request, 'Post has been unsaved!')
        return redirect('post-detail', pk=id)
      else:
        Unsave.objects.create_unsave(self.get_object(), request.user)
        # messages.success(request, 'Post has been unsaved!')
        return redirect('post-detail', pk=id)


###### FOLLOW #######
@require_POST
@login_required
def user_follow(request):
    user_id = request.POST.get('id')
    action = request.POST.get('action')
    if user_id and action:
        try:
            user = User.objects.get(id=user_id)
            if action == 'follow':
                Contact.objects.get_or_create(user_from=request.user,
                                              user_to=user)
                create_action(request.user, 'followed', user)

            else:
                Contact.objects.filter(user_from=request.user,
                                       user_to=user).delete()
               
            return JsonResponse({'status':'ok'})
        except User.DoesNotExist:
            return JsonResponse({'status':'ko'})
    return JsonResponse({'status':'ko'})


class RssDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = UserRss
    success_url = "/"

    def test_func(self):
        userrss = self.get_object()
        if self.request.user == userrss.feed_author:
            return True
        return False

def search(request):
    template = 'blog/home_dev.html'
    query = request.GET.get('q')
    results = Post.objects.filter(Q(title__icontains=query) | Q(content__icontains=query))
    pages = pagination(request, results, num=5)

    feed = hacker_news_feed()

    if request.user.is_authenticated:

      actions = get_actions(request)

      context = {
          'posts':pages[0],
          'page_range':pages[1],
          'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted')[:6],
          'discussion': Post.objects.filter(tags__tag_name='discussion')[:6],
          'likes': Like.objects.filter(owner=request.user).order_by('-liked_time')[:5],
          'saved': Save.objects.filter(owner=request.user).order_by('-liked_time')[:5],
          'actions': actions, 
          'feed':feed
      }
    
    else:
      context = {
          'posts':pages[0],
          'page_range':pages[1],
          'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted')[:6],
          'discussion': Post.objects.filter(tags__tag_name='discussion')[:6],
          'feed':feed
      }

    return render(request, template, context)


def filterr(request):
    template = 'blog/home_dev.html'

    user = request.user

    if user.is_authenticated:

      get_likes = Like.objects.filter(owner=request.user).order_by('-liked_time')[:5]
      get_saved = Save.objects.filter(owner=request.user).order_by('-liked_time')[:5]
      actions = get_actions(request)

    feed = hacker_news_feed()

    if user.is_authenticated:

      context = {
          'django': Post.objects.filter(tags__tag_name='django').order_by('-date_posted'),
          'selenium': Post.objects.filter(tags__tag_name='selenium').order_by('-date_posted'),
          'react': Post.objects.filter(tags__tag_name='react').order_by('date_posted'),
          'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted'),
          'discussion': Post.objects.filter(tags__tag_name='discussion'),
          'maths': Post.objects.filter(tags__tag_name='maths').order_by('date_posted'),
          'javascript': Post.objects.filter(tags__tag_name='js').order_by('date_posted'),
          'java': Post.objects.filter(tags__tag_name='java').order_by('date_posted'),
          'algorithms': Post.objects.filter(tags__tag_name='algorithms').order_by('date_posted'),
          'cybersecurity': Post.objects.filter(tags__tag_name='cyber security').order_by('date_posted'),
          'chsarp': Post.objects.filter(tags__tag_name='csharp').order_by('date_posted'),
          'desc_django': Tags.objects.get(tag_name='django'),
          'desc_python': Tags.objects.get(tag_name='python'),
          'desc_selenium': Tags.objects.get(tag_name='selenium'),
          'desc_react': Tags.objects.get(tag_name='react'),
          'desc_java': Tags.objects.get(tag_name='java'),
          'desc_csharp': Tags.objects.get(tag_name='csharp'),
          'desc_js': Tags.objects.get(tag_name='js'),
          'likes': get_likes,
          'saved': get_saved,     
          'actions':actions,
          'feed':feed 
      }
      
      return render(request, template, context)

    else:
      context = {
          'django': Post.objects.filter(tags__tag_name='django').order_by('-date_posted'),
          'selenium': Post.objects.filter(tags__tag_name='selenium').order_by('-date_posted'),
          'react': Post.objects.filter(tags__tag_name='react').order_by('date_posted'),
          'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted'),
          'discussion': Post.objects.filter(tags__tag_name='discussion'),
          'maths': Post.objects.filter(tags__tag_name='maths').order_by('date_posted'),
          'javascript': Post.objects.filter(tags__tag_name='js').order_by('date_posted'),
          'java': Post.objects.filter(tags__tag_name='java').order_by('date_posted'),
          'algorithms': Post.objects.filter(tags__tag_name='algorithms').order_by('date_posted'),
          'cybersecurity': Post.objects.filter(tags__tag_name='cyber security').order_by('date_posted'),
          'chsarp': Post.objects.filter(tags__tag_name='csharp').order_by('date_posted'),
          'desc_django': Tags.objects.get(tag_name='django'),
          'desc_python': Tags.objects.get(tag_name='python'),
          'desc_selenium': Tags.objects.get(tag_name='selenium'),
          'desc_react': Tags.objects.get(tag_name='react'),
          'desc_java': Tags.objects.get(tag_name='java'),
          'desc_csharp': Tags.objects.get(tag_name='csharp'),
          'desc_js': Tags.objects.get(tag_name='js'),    
          'feed':feed 
      }

    return render(request, template, context)

@login_required
def news(request):

    rss = UserRss.objects.filter(feed_author=request.user)

    if request.GET.get("url"):
        url = request.GET["url"] #Getting URL
        feed = feedparser.parse(url) #Parsing XML data

    else:
        feed = None

    actions = get_actions(request)

    context = {
        'feed': feed,
        'rss': rss,
        'likes': Like.objects.filter(owner=request.user).order_by('-liked_time')[:5],
        'saved': Save.objects.filter(owner=request.user).order_by('-liked_time')[:5],
        'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted')[:6],
        'discussion': Post.objects.filter(tags__tag_name='discussion')[:6],
        'actions':actions
    }

    return render(request, 'blog/news.html', context)


def contact(request):
    user = request.user
    feed = hacker_news_feed()

    if request.method == "GET":
      form = ContactForm()
    else:
      form = ContactForm(request.POST)
      if form.is_valid():
        subject = form.cleaned_data['subject']
        from_email = form.cleaned_data['from_email']
        message = form.cleaned_data['message']
        send_mail(subject, message, from_email, ['jeremy.faret@gmail.com'])
          
    if user.is_authenticated:
      actions = get_actions(request)

      return render(request, 'blog/contact.html', {'title': 'Contact',
                                                  'actions':actions,
                                                  'likes': Like.objects.filter(owner=request.user).order_by('-liked_time')[:5],
                                                  'saved': Save.objects.filter(owner=request.user).order_by('-liked_time')[:5],
                                                  'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted')[:6],
                                                  'discussion': Post.objects.filter(tags__tag_name='discussion')[:6],
                                                  'form': form,
                                                  'feed':feed
                                                  })
    else:
      return render(request, 'blog/contact.html', {'title': 'Contact',
                                                  'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted')[:6],
                                                  'discussion': Post.objects.filter(tags__tag_name='discussion')[:6],
                                                  'form': form,
                                                  'feed':feed
                                                  })

def about(request):
    about = AboutPage.objects.get(id=1)
    user = request.user
    feed = hacker_news_feed()

    if user.is_authenticated :
      actions = get_actions(request)
      return render(request, 'blog/about.html', {'about': about,
                                                'actions':actions,
                                                'likes': Like.objects.filter(owner=request.user).order_by('-liked_time')[:5],
                                                'saved': Save.objects.filter(owner=request.user).order_by('-liked_time')[:5],
                                                'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted')[:6],
                                                'discussion': Post.objects.filter(tags__tag_name='discussion')[:6],
                                                'feed':feed
                                                })
    else:
      return render(request, 'blog/about.html', {'about': about,
                                                'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted')[:6],
                                                'discussion': Post.objects.filter(tags__tag_name='discussion')[:6],
                                                'feed':feed
                                                })


def gone(request, *args, **kwargs):
    """
    Display a nice 410 gone page.
    """
    return render(request, 'blog/410.html', status=410)


##### Polls #####

# Get questions and display them
def pollindex(request):
    user = request.user
    
    latest_question_list = PollQuestion.objects.order_by('-pub_date')[:5]
    feed = hacker_news_feed()
    
    if user.is_authenticated:
      actions = get_actions(request)
      context = {
        'latest_question_list': latest_question_list,
        'likes': Like.objects.filter(owner=request.user).order_by('-liked_time')[:5],
        'saved': Save.objects.filter(owner=request.user).order_by('-liked_time')[:5],
        'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted')[:6],
        'discussion': Post.objects.filter(tags__tag_name='discussion')[:6],
        'actions':actions,
        'feed':feed
        }
      return render(request, 'blog/polls/polls-index.html', context)
    else:
      context = {
        'latest_question_list': latest_question_list,
        'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted')[:6],
        'discussion': Post.objects.filter(tags__tag_name='discussion')[:6],
        'feed':feed
        }
      return render(request, 'blog/polls/polls-index.html', context)


# Show specific question and choices
def polldetail(request, question_id):
  feed = hacker_news_feed()
  user = request.user

  try:
    question = PollQuestion.objects.get(pk=question_id)
  except PollQuestion.DoesNotExist:
    raise Http404("Question does not exist")

  if user.is_authenticated:
    actions = get_actions(request)

    return render(request, 'blog/polls/polls-details.html', { 'question': question,
                                                            'feed':feed,
                                                            'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted')[:6],
                                                            'discussion': Post.objects.filter(tags__tag_name='discussion')[:6], 
                                                            'likes': Like.objects.filter(owner=request.user).order_by('-liked_time')[:5],
                                                            'saved': Save.objects.filter(owner=request.user).order_by('-liked_time')[:5],
                                                            'actions':actions,
                                                            })
  else:
    return render(request, 'blog/polls/polls-details.html', {'question': question,
                                                            'feed':feed,
                                                            'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted')[:6],
                                                            'discussion': Post.objects.filter(tags__tag_name='discussion')[:6], 
                                                            })

# Get question and display results
@login_required
def pollresults(request, question_id):
  feed = hacker_news_feed()

  question = get_object_or_404(PollQuestion, pk=question_id)

  if request.user.is_autenticated:
    actions = get_actions(request)

    return render(request, 'blog/polls/polls-results.html', { 'question': question,
                                                              'likes': Like.objects.filter(owner=request.user).order_by('-liked_time')[:5],
                                                              'saved': Save.objects.filter(owner=request.user).order_by('-liked_time')[:5],
                                                              'feed':feed,
                                                              'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted')[:6],
                                                              'discussion': Post.objects.filter(tags__tag_name='discussion')[:6],
                                                              'actions':actions,
                                                              })
  else:
    return render(request, 'blog/polls/polls-results.html', { 'question': question,
                                                              'feed':feed,
                                                              'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted')[:6],
                                                              'discussion': Post.objects.filter(tags__tag_name='discussion')[:6],
                                                              })

# Vote for a question choice
def pollvote(request, question_id):
    feed = hacker_news_feed()
    question = get_object_or_404(PollQuestion, pk=question_id)

    try:
        selected_choice = question.pollchoice_set.get(pk=request.POST['choice'])
    except (KeyError, PollChoice.DoesNotExist):
        # Redisplay the question voting form.

        if request.user.is_authenticated:
          actions = get_actions(request)

          return render(request, 'blog/polls/polls-details.html', {
              'question': question,
              'error_message': "You didn't select a choice.",
              'likes': Like.objects.filter(owner=request.user).order_by('-liked_time')[:5],
              'saved': Save.objects.filter(owner=request.user).order_by('-liked_time')[:5],
              'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted')[:6],
              'discussion': Post.objects.filter(tags__tag_name='discussion')[:6],
              'feed':feed,
              'actions':actions
          })

        else:
          return render(request, 'blog/polls/polls-details.html', {
              'question': question,
              'error_message': "You didn't select a choice.",
              'python': Post.objects.filter(tags__tag_name='python').order_by('-date_posted')[:6],
              'discussion': Post.objects.filter(tags__tag_name='discussion')[:6],
              'feed':feed
          })

    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('pollresults', args=(question.id,)))