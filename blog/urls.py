from functools import partial
from django.views import defaults

from django.urls import path
from . import views
from .views import (PostListView, PostCreateView, PostUpdateView, 
                    PostDeleteView, UserPostListView, search, filterr, pollindex, 
                    polldetail, pollresults, pollvote, user_detail, user_list, 
                    post_detail, gone, PostLikeView, PostUnlikeView,
                    PostSaveView, PostUnSaveView)

urlpatterns = [
    path('', PostListView.as_view(), name='blog-home'),
    # display post from certain user
    path('user/<str:username>', UserPostListView.as_view(), name='user-posts'),
    # view the post
    path('post/<int:pk>', views.post_detail, name='post-detail'),
    # create a new post
    path('post/new/', PostCreateView.as_view(), name='post-create'),
    # update a post
    path('post/<int:pk>/update/', PostUpdateView.as_view(), name='post-update'),
    # delete a post 
    path('post/<int:pk>/delete/', PostDeleteView.as_view(), name='post-delete'),

    # like, dislike posts
    path('<int:id>/like/', PostLikeView.as_view(), name='post-likes'),
    path('<int:id>/unlike/', PostUnlikeView.as_view(), name='post-unlikes'),

    # save, unsave posts
    path('<int:id>/save/', PostSaveView.as_view(), name='post-save'),
    path('<int:id>/unsave/', PostUnSaveView.as_view(), name='post-unsave'),

    # search 
    path('results/', views.search, name='search'),
    # path('results/users/', views.search_user, name='search-user'),
    
    # profile user detail
    path('profile/<username>', views.user_detail, name='user-detail'),

    # following users
    path('users/follow/', views.user_follow, name='user_follow'),

    # filters
    path('filter/<str:tags>', PostListView.as_view(), name='filter'),
    path('filter/selenium/', views.filterr, name='filter-sele'),
    path('filter/django/', views.filterr, name='filter-django'),
    path('filter/react/', views.filterr, name='filter-react'),
    path('filter/python/', views.filterr, name='filter-python'),
    path('filter/maths/', views.filterr, name='filter-maths'),
    path('filter/java/', views.filterr, name='filter-java'),
    path('filter/js/', views.filterr, name='filter-javascript'),
    path('filter/algorithms/', views.filterr, name='filter-algo'),
    path('filter/csharp/', views.filterr, name='filter-csharp'),
    path('filter/discussion/', views.filterr, name='filter-discussion'),
    path('filter/cybersec/', views.filterr, name='filter-cybersec'),

    # polls
    path('polls/', views.pollindex, name='pollindex'),
    path('polls/<int:question_id>/', views.polldetail, name='polldetail'),
    path('polls/<int:question_id>/results/', views.pollresults, name='pollresults'),
    path('polls/<int:question_id>/vote/', views.pollvote, name='pollvote'),

    # users list
    path('users/', views.user_list, name="users-list"),

    path('news/', views.news, name='news'),
    path('about/', views.about, name='blog-about'),
    path('contact/', views.contact, name='blog-contact'),

    path('400/', partial(defaults.bad_request, exception=None)),
    path('403/', partial(defaults.permission_denied, exception=None)),
    path('404/', partial(defaults.page_not_found, exception=None)),
    path('410/', gone),
    path('500/', defaults.server_error),
]
